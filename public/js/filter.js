/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 74);
/******/ })
/************************************************************************/
/******/ ({

/***/ 74:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(75);


/***/ }),

/***/ 75:
/***/ (function(module, exports) {

$(function () {

  var filterTimeout = null;

  // Check sort filter on start
  checkSort();

  // Set order_by filter on click
  $('.filter-order').click(function () {
    var sortInput = $('#filter-form input[name=sort]');

    // Toggle sort direction icon if have selected filter
    if ($(this).find('input').is(':checked')) {
      if (sortInput.val() === 'asc') {
        sortInput.val('desc');

        $(this).find('.sort-direction').html('<i class="fas fa-sort-down"></i>');
      } else {
        sortInput.val('asc');

        $(this).find('.sort-direction').html('<i class="fas fa-sort-up"></i>');
      }

      // Apply filters
      applyFilters();
    } else {
      // Clear icons and set default direction and icon
      $('#filter-form .sort-direction').html('');

      sortInput.val('asc');

      $(this).find('.sort-direction').html('<i class="fas fa-sort-up"></i>');
    }
  });

  // Apply filters on change inputs
  $('.filter-input').change(function () {
    applyFilters();
  });

  // Apply filter
  function applyFilters() {
    // Clear timer if isset
    if (filterTimeout) clearTimeout(filterTimeout);

    // Set timer
    filterTimeout = setTimeout(function () {
      // Submit form
      $('#filter-form').submit();

      // Clear timer
      clearTimeout(filterTimeout);
    }, 1000);
  }

  // Check sort filter
  function checkSort() {
    var filterOrders = $('#filter-form .filter-order');

    // If filter order_by not selected
    if (!filterOrders.find('input:checked').length) {
      var inputCreated = filterOrders.find('input[value=created_at]');

      // Set created_by as selected
      inputCreated.parent().addClass('active');
      inputCreated.siblings('.sort-direction').html('<i class="fas fa-sort-down"></i>');
    } else {
      // Add sort direction icon if have selected order_by filter
      filterOrders.each(function () {
        if ($(this).find('input').is(':checked')) {
          if ($('#filter-form input[name=sort]').val() === 'asc') {
            $(this).find('.sort-direction').html('<i class="fas fa-sort-up"></i>');
          } else {
            $(this).find('.sort-direction').html('<i class="fas fa-sort-down"></i>');
          }
        }
      });
    }
  }
});

/***/ })

/******/ });