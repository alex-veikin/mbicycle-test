require('./url_slug')

$(function () {

  getSelectedCategories()

  // Generate URL slug
  $('#title').on('input', function () {
    $('#slug').val(window.url_slug($('#title').val()))
  })

  // Update selected categories count
  $('.category-group input:checkbox').change(function () {
    getSelectedCategories()
  })

  // Add field to list and hidden input
  $('.field-add-button').click(function (event) {
    event.preventDefault()

    let title = $('.field-title-input')
    let value = $('.field-value-input')

    if (title.val() && value.val()) {
      $('.fields-list').append(`
<li class="list-group-item p-1 d-flex justify-content-between align-items-center">
    <span>${title.val()}: ${value.val()}</span>
    <span class="badge badge-secondary badge-pill delete-field" style="cursor: pointer;">Delete</span>
    <input type="hidden" name="fields[${title.val()}]" value="${value.val()}">
</li>`)

      title.val('')
      value.val('')
    }
  })

  // Delete attribute
  $('.fields-list').on('click', '.list-group-item', function (event) {
    if ($(event.target).hasClass('delete-field')) {
      $(this).remove()
    }
  })

  // Submit delete form
  $('.delete-link').click(function (event) {
    event.preventDefault()

    if (confirm('Are you sure to delete?')) {
      $(this).siblings('.delete-form').submit()
    }
  })

  function getSelectedCategories () {
    let total = 0

    $('.category-group').each(function () {
      let count = $(this).find($('input:checkbox:checked')).length

      $(this).find('.selected-categories').text(count)

      total += count
    })

    $('.selected-categories-total').text(total)
  }
})