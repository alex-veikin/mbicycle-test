$(function () {

  let filterTimeout = null

  // Check sort filter on start
  checkSort()

  // Set order_by filter on click
  $('.filter-order').click(function () {
    let sortInput = $('#filter-form input[name=sort]')

    // Toggle sort direction icon if have selected filter
    if ($(this).find('input').is(':checked')) {
      if (sortInput.val() === 'asc') {
        sortInput.val('desc')

        $(this).find('.sort-direction').html('<i class="fas fa-sort-down"></i>')
      } else {
        sortInput.val('asc')

        $(this).find('.sort-direction').html('<i class="fas fa-sort-up"></i>')
      }

      // Apply filters
      applyFilters()
    } else { // Clear icons and set default direction and icon
      $('#filter-form .sort-direction').html('')

      sortInput.val('asc')

      $(this).find('.sort-direction').html('<i class="fas fa-sort-up"></i>')
    }
  })

  // Apply filters on change inputs
  $('.filter-input').change(function () {
    applyFilters()
  })

  // Apply filter
  function applyFilters () {
    // Clear timer if isset
    if (filterTimeout) clearTimeout(filterTimeout)

    // Set timer
    filterTimeout = setTimeout(() => {
      // Submit form
      $('#filter-form').submit()

      // Clear timer
      clearTimeout(filterTimeout)
    }, 1000)
  }

  // Check sort filter
  function checkSort () {
    let filterOrders = $('#filter-form .filter-order')

    // If filter order_by not selected
    if (!filterOrders.find('input:checked').length) {
      let inputCreated = filterOrders.find('input[value=created_at]')

      // Set created_by as selected
      inputCreated.parent().addClass('active')
      inputCreated.siblings('.sort-direction').html('<i class="fas fa-sort-down"></i>')
    } else {
      // Add sort direction icon if have selected order_by filter
      filterOrders.each(function () {
        if ($(this).find('input').is(':checked')) {
          if ($('#filter-form input[name=sort]').val() === 'asc') {
            $(this).find('.sort-direction').html('<i class="fas fa-sort-up"></i>')
          } else {
            $(this).find('.sort-direction').html('<i class="fas fa-sort-down"></i>')
          }
        }
      })
    }
  }

})