<ul class="nav flex-column accordion" id="accordionNav{{ $id ?? 0 }}">
    @foreach($categories as $category)
        <li class="nav-item mt-1 mr-1">
            <div class="d-flex justify-content-between">
                {{ link_to_route('site.categories.show', $category->title, ['segments' => $category->urlSegments()],
                    ['class' => ['nav-link', (request('segments') === $category->urlSegments()) ? 'active' : '']])
                }}

                @if($category->children->count())
                    <button class="btn btn-sm rounded" data-toggle="collapse"
                            data-target="#collapse{{ $loop->iteration . '-' . ($id ?? 0) }}"
                            aria-expanded="true" aria-controls="collapse{{ $loop->iteration . '-' . ($id ?? 0) }}">
                        +
                    </button>
                @endif
            </div>

            <div id="collapse{{ $loop->iteration . '-' . ($id ?? 0) }}"
                 class="nav flex-column ml-3 collapse {{ (request('segments') === $category->urlSegments()) ? 'show' : '' }}"
                 aria-labelledby="headingOne" data-parent="#accordionNav{{ $id ?? 0 }}">
                @if($category->children->count())
                    @include('components.menu',
                     ['categories' => $category->children, 'id' => $category->id, 'segments' => $category->urlSegments()])
                @endif
            </div>
        </li>
    @endforeach
</ul>