<div class="mb-3 d-flex justify-content-between">
    <form action="{{ url()->current() }}" id="filter-form">
        <label class="mr-3 my-1">
            With image:
            <input class="filter-input" type="checkbox" name="image" value="1" {{ request('image') ? 'checked' : '' }}>
        </label>

        <div class="d-inline-block mr-3 my-1">
            Order by:

            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                <label class="btn btn-secondary btn-sm filter-order {{ request('order_by') === 'title' ? 'active' : '' }}">
                    <input class="filter-input" type="radio" name="order_by" value="title"
                            {{ request('order_by') === 'title' ? 'checked' : '' }}>
                    Title <span class="sort-direction"></span>
                </label>

                <label class="btn btn-secondary btn-sm filter-order {{ request('order_by') === 'price' ? 'active' : '' }}">
                    <input class="filter-input" type="radio" name="order_by" value="price"
                            {{ request('order_by') === 'price' ? 'checked' : '' }}>
                    Price <span class="sort-direction"></span>
                </label>

                <label class="btn btn-secondary btn-sm filter-order {{ request('order_by') === 'created_at' ? 'active' : '' }}">
                    <input class="filter-input" type="radio" name="order_by" value="created_at"
                            {{ request('order_by') === 'created_at' ? 'checked' : '' }}>
                    Created <span class="sort-direction"></span>
                </label>
            </div>
        </div>

        <input type="hidden" name="sort" value="{{ request('sort') ?? 'desc' }}">

        <label class="mr-3 my-1">
            By keywords:
            <input class="filter-input" type="text" name="search" value="{{ request('search') ?? '' }}">
        </label>

        {{--<input type="submit" value="Done">--}}
    </form>
</div>