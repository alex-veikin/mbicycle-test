<div class="card text-center h-100">
    <img class="card-img-top img-thumbnail" src="{{ $product->image_src }}" alt="{{ $product->title }}"
         style="display: block;width: 100%;height: auto;">
    <div class="card-body d-flex flex-column">
        <h5 class="card-title">{{ $product->title }}</h5>

        <div class="card-text">
            <p>{{ $product->content }}</p>

            @if($product->price)
                <p>Price: ${{ $product->price }}</p>
            @endif
        </div>

        @if(request()->route()->getName() === 'site.categories.show')
            {{ link_to_route('site.categories.show', 'Show details',
             ['segments' => ($category->urlSegments() . '/' . $product->slug)], ['class' => 'mt-auto']) }}
        @else
            {{ link_to_route('site.products.show', 'Show details', ['product' => $product->slug], ['class' => 'mt-auto']) }}
        @endif
    </div>
</div>