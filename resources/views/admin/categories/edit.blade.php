@extends('layouts.admin')

@section('title','Edit ' . ($category->parent_id ? 'subcategory' : 'category'))

@section('content')
    <div class="card">
        <div class="card-header">
            @if($category->parent_id)
                {{ link_to_route('admin.categories.show', '<', ['category' => $category->parent_id], ['class' => 'btn btn-secondary btn-sm mr-2']) }}
                Edit subcategory - {{ $category->title }}
            @else
                {{ link_to_route('admin.categories.index', '<', null, ['class' => 'btn btn-secondary btn-sm mr-2']) }}
                Edit category - {{ $category->title }}
            @endif
        </div>

        <div class="card-body">
            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('success') }}

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            {!! Form::open()->route('admin.categories.update', ['category' => $category])->fill($category)->method('patch')->multipart() !!}

            @if($category->parent_id)
                {!! Form::hidden('parent_id') !!}
            @endif

            {!! Form::text('title', 'Title *')->placeholder('Enter title')->required() !!}
            {!! Form::text('slug', 'Url slug (example: my-first-category) *')->placeholder('Enter url slug')->required() !!}

            <div class="d-flex justify-content-center">
                <img class="card-img-top img-thumbnail" src="{{ $category->image_src }}" alt="{{ $category->title }}"
                     style="display: block;max-width: 300px;width: 100%;height: auto;">
            </div>

            {!! Form::file('file', $category->image ? 'Change image' : 'Add image')->required(false) !!}
            {!! Form::submit('Update category') !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection