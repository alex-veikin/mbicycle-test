@extends('layouts.admin')

@section('title', 'Category - ' . $category->title)

@section('content')
    <div class="card">
        <div class="card-header d-flex justify-content-between">
            <span>
                {{ link_to_route('admin.categories.index', '<', null, ['class' => 'btn btn-secondary btn-sm mr-2']) }}

                Category - {{ $category->title }}
            </span>

            {{ link_to_route('admin.categories.create', 'Add subcategory', ['parent' => $category->id], ['class' => 'btn btn-secondary btn-sm']) }}
        </div>

        <div class="card-body">
            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('success') }}

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            @if($category->children->count())
                <p>Subcategories:</p>
                <table class="table table-striped table-bordered table-hover table-sm mb-4">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Image</th>
                        <th scope="col">Title</th>
                        <th scope="col">Created</th>
                        <th scope="col">Edit</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($category->children as $subcategory)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>
                                <img src="{{ $subcategory->image_src }}" alt="{{ $subcategory->title }}" style="width: 80px">
                            </td>
                            <td>{{ $subcategory->title }}</td>
                            <td class="small">{{ $subcategory->created_at }}</td>
                            <td>
                                <a href="{{ route('admin.categories.edit', ['category' => $subcategory]) }}"
                                   class="mr-1 small">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>

                                <a href="#" class="small delete-link">
                                    <i class="far fa-trash-alt"></i>
                                </a>

                                {!! Form::open()->route('admin.categories.destroy', ['category' => $subcategory])
                                ->method('delete')
                                ->attrs(['style' => 'display: none;', 'class' => 'delete-form'])
                                 !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <p>No subcategories</p>
            @endif
        </div>
    </div>
@endsection