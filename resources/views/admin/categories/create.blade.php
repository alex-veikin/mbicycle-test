@extends('layouts.admin')

@section('title','Add new ' . (isset($category) ? 'subcategory' : 'category'))

@section('content')
    <div class="card">
        <div class="card-header">
            @if(isset($category))
                {{ link_to_route('admin.categories.show', '<', ['category' => $category], ['class' => 'btn btn-secondary btn-sm mr-2']) }}
                Add new subcategory
            @else
                {{ link_to_route('admin.categories.index', '<', null, ['class' => 'btn btn-secondary btn-sm mr-2']) }}
                Add new category
            @endif
        </div>

        <div class="card-body">
            {!! Form::open()->route('admin.categories.store')->multipart() !!}

            @if(isset($category))
                {!! Form::hidden('parent_id')->value($category->id) !!}
            @endif

            {!! Form::text('title', 'Title *')->placeholder('Enter title')->required() !!}
            {!! Form::text('slug', 'Url slug (example: my-first-category) *')->placeholder('Enter url slug')->required() !!}
            {!! Form::file('file', 'Add image')->required(false) !!}
            {!! Form::submit('Save category') !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection