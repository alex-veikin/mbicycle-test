@extends('layouts.admin')

@section('title', 'Categories')

@section('content')
    <div class="card">
        <div class="card-header d-flex justify-content-between">
            <span>Categories</span>

            {{ link_to_route('admin.categories.create', 'Add category', null, ['class' => 'btn btn-secondary btn-sm']) }}
        </div>

        <div class="card-body">
            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('success') }}

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            @if($categories->count())
                <table class="table table-striped table-bordered table-hover table-sm mb-4">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Image</th>
                        <th scope="col">Title</th>
                        <th scope="col">Subcategories</th>
                        <th scope="col">Created</th>
                        <th scope="col">Edit</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>
                                <img src="{{ $category->image_src }}" alt="{{ $category->title }}" style="width: 80px">
                            </td>
                            <td>{{ link_to_route('admin.categories.show', $category->title, ['category' => $category]) }}</td>
                            <td>
                                @foreach($category->children as $subcategory)
                                    <span class="d-block border border-secondary rounded ml-1 mb-1 px-1 small">
                                    {{ $subcategory->title }}
                                </span>
                                @endforeach
                            </td>
                            <td class="small">{{ $category->created_at }}</td>
                            <td>
                                <a href="{{ route('admin.categories.edit', ['category' => $category]) }}"
                                   class="mr-1 small">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>

                                <a href="#" class="small delete-link">
                                    <i class="far fa-trash-alt"></i>
                                </a>

                                {!! Form::open()->route('admin.categories.destroy', ['category' => $category])
                                ->method('delete')
                                ->attrs(['style' => 'display: none;', 'class' => 'delete-form'])
                                 !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <p>No categories</p>
            @endif
        </div>
    </div>
@endsection