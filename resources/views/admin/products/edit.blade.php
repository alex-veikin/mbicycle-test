@extends('layouts.admin')

@section('title','Edit product')

@section('content')
    <div class="card">
        <div class="card-header">
            {{ link_to_route('admin.products.index', '<', null, ['class' => 'btn btn-secondary btn-sm mr-2']) }}
            Edit product - {{ $product->title }}
        </div>

        <div class="card-body">
            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('success') }}

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            {!! Form::open()->route('admin.products.update', ['product' => $product])->fill($product)->method('patch')->multipart() !!}
            {!! Form::text('title', 'Title *')->placeholder('Enter title')->required() !!}
            {!! Form::text('slug', 'Url slug (example: my-first-product) *')->placeholder('Enter url slug')->required() !!}
            {!! Form::textarea('content', 'Content *')->required() !!}
            {!! Form::text('price', 'Price (example: 5.20) *')->type('number')
            ->placeholder('Enter price')->attrs(['step' => '0.01'])->required() !!}

            <div class="d-flex justify-content-center">
                <img class="card-img-top img-thumbnail" src="{{ $product->image_src }}" alt="{{ $product->title }}"
                     style="display: block;max-width: 300px;width: 100%;height: auto;">
            </div>

            {!! Form::file('file', $product->image ? 'Change image' : 'Add image')->required(false) !!}

            <div class="attributes-label">Attributes</div>
            <div class="attributes-group mb-3">
                <div class="attributes-add d-flex justify-content-between align-items-center">
                    <span>
                        Title:
                        <input type="text" class="attribute-title-input small">
                    </span>
                    <span>
                        Value:
                        <input type="text" class="attribute-value-input small">
                    </span>
                    <button class="btn btn-sm attribute-add-button">Add new attribute</button>
                </div>

                <ul class="fields-list list-group mt-2">
                    @if(old('fields'))
                        @foreach(old('fields') as $title => $value)
                            <li class="list-group-item p-1 d-flex justify-content-between align-items-center">
                                <span>{{ $title }}: {{ $value }}</span>
                                <span class="badge badge-secondary badge-pill delete-field"
                                      style="cursor: pointer;">
                                    Delete
                                </span>
                                <input type="hidden" name="fields[{{ $title }}]" value="{{ $value }}">
                            </li>
                        @endforeach
                    @endif

                    @if($product->attributes)
                        @foreach($product->attributes as $attribute)
                            <li class="list-group-item p-1 d-flex justify-content-between align-items-center">
                                <span>{{ $attribute->title }}: {{ $attribute->description }}</span>
                                <span class="badge badge-secondary badge-pill delete-field"
                                      style="cursor: pointer;">
                                    Delete
                                </span>
                                <input type="hidden" name="fields[{{ $attribute->title }}]"
                                       value="{{ $attribute->description }}">
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>

            <div class="categories-label d-flex justify-content-between align-items-center mb-2">
                <span>Categories</span>

                <span class="badge badge-secondary badge-pill">
                    Total selected: <span class="selected-categories-total">0</span>
                </span>
            </div>
            <ul class="list-group accordion mb-3" id="accordionCategories">
                @foreach($categories as $category)
                    <li class="list-group-item category-group p-2 bg-light">
                        <div class="d-flex justify-content-between align-items-center"
                             id="heading{{ $loop->iteration }}" data-toggle="collapse"
                             data-target="#collapse{{ $loop->iteration }}"
                             aria-controls="collapse{{ $loop->iteration }}"
                        >
                            <span>{{ $category->title }}</span>

                            <span class="badge badge-secondary badge-pill">
                                Selected: <span class="selected-categories">0</span>
                            </span>
                        </div>

                        <div id="collapse{{ $loop->iteration }}"
                             class="collapse mt-1 ml-3"
                             aria-labelledby="heading{{ $loop->iteration }}"
                             data-parent="#accordionCategories">
                            @foreach($category->children as $subcategory)
                                @if(in_array($subcategory->id, $array = old('product_categories') ?? [])
                                 || $product->categories->find($subcategory->id))
                                    {!!
                                     Form::checkbox('product_categories[]', $subcategory->title, $subcategory->id)
                                     ->id('category-' . $subcategory->id)
                                     ->required(false)
                                     ->attrs(['checked' => true])
                                    !!}
                                @else
                                    {!!
                                     Form::checkbox('product_categories[]', $subcategory->title, $subcategory->id)
                                     ->id('category-' . $subcategory->id)
                                     ->required(false)
                                     ->checked(false)
                                    !!}
                                @endif
                            @endforeach
                        </div>
                    </li>
                @endforeach
            </ul>

            {!! Form::submit('Update product') !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection