@extends('layouts.admin')

@section('title',
'Products'
 . ($products->lastPage() > 1 ? (' - page ' . $products->currentPage()) : '')
 )

@section('content')
    <admin-products url="{{ url()->current() }}" path="{{ request()->path() }}"/>
@endsection