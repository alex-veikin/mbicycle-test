@extends('layouts.admin')

@section('title', 'Parser')

@section('content')
    <div class="card">
        <div class="card-header d-flex justify-content-between">
            Parse resources

            {{ link_to_route('admin.parse_items.create', 'Add new resource') }}
        </div>

        <div class="card-body">
            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('success') }}

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            <ul class="list-group">
                @forelse($parse_items as $item)
                    <li class="list-group-item">
                        <div><b>{{ $item->title }}</b></div>
                        <div class="small"><b>Type: </b><i>{{ $item->type }}</i></div>
                        <div class="small"><b>URL: </b><i>{{ $item->url }}</i></div>
                        <div class="small"><b>Item selector: </b><i>"{{ $item->selector }}"</i></div>
                        <div class="small"><b>Fields: </b>
                            @foreach($item->fields_array as $title => $selector)
                                <div>{{ $title }} => <i>"{{ $selector }}"</i></div>
                            @endforeach
                        </div>
                        <div class="d-flex justify-content-between">
                            <a href="{{ route('admin.parse_items.show', ['parse_item' => $item]) }}">Parse</a>

                            <div>
                                <a href="{{ route('admin.parse_items.edit', ['parse_item' => $item]) }}">Edit</a>

                                <a href="#" class="delete-link">Delete</a>

                                {!! Form::open()->route('admin.parse_items.destroy', ['parse_item' => $item])
                                ->method('delete')
                                ->attrs(['style' => 'display: none;', 'class' => 'delete-form'])
                                 !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </li>
                @empty
                    <p>No resources</p>
                @endforelse
            </ul>
        </div>
    </div>
@endsection