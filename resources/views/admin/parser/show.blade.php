@extends('layouts.admin')

@section('title', 'Parser')

@section('content')
    <div class="card">
        <div class="card-header">Parse source "{{ $parse_item->title }}"</div>

        <div class="card-body">
            <ul class="list-group">
                @foreach($items as $item)
                    <li class="list-group-item">
                        @foreach($item as $key => $value)
                            <div><b>{{ $key }}: </b><i>{{ $value }}</i></div>
                        @endforeach

                        {{ link_to_route('admin.' . $parse_item->type . '.create', 'Add to ' . $parse_item->type, $item) }}
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection