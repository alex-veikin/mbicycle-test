@extends('layouts.admin')

@section('title', 'Parser')

@section('content')
    <div class="card">
        <div class="card-header">
            {{ link_to_route('admin.parse_items.index', '<', null, ['class' => 'btn btn-secondary btn-sm mr-2']) }}
            New parser resource
        </div>

        <div class="card-body">
            {!! Form::open()->method('post')->route('admin.parse_items.store') !!}
            {!! Form::text('title', 'Title *')->placeholder('Enter title')->required() !!}
            {!! Form::text('type', 'Type (example: products or categories)*')->placeholder('Enter type')->required() !!}
            {!! Form::text('url', 'Resource URL *')->placeholder('Enter resource url')->required() !!}
            {!! Form::text('selector', 'Item selector *')->placeholder('Enter item selector')->required() !!}

            <div class="fields-label">Fields</div>
            <div class="fields-group mb-3">
                <div class="fields-add d-flex justify-content-between align-items-center">
                    <span>Title: <input type="text" class="field-title-input small"></span>
                    <span>Value: <input type="text" class="field-value-input small"></span>
                    <button class="btn btn-sm field-add-button">Add new field</button>
                </div>

                <ul class="fields-list list-group mt-2">
                    @if(old('fields'))
                        @foreach(old('fields') as $title => $value)
                            <li class="list-group-item p-1 d-flex justify-content-between align-items-center">
                                <span>{{ $title }}: {{ $value }}</span>
                                <span class="badge badge-secondary badge-pill delete-field" style="cursor: pointer;">Delete</span>
                                <input type="hidden" name="fields[{{ $title }}]" value="{{ $value }}">
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>

            {!! Form::submit('Save resource') !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection