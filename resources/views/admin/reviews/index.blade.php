@extends('layouts.admin')

@section('title', 'Reviews')

@section('content')
    <div class="card">
        <div class="card-header d-flex justify-content-between">
            Reviews
        </div>

        <div class="card-body">
            <table class="table table-striped table-bordered table-hover table-sm mb-4">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Product</th>
                    <th scope="col">User ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Rating</th>
                    <th scope="col">Text</th>
                    <th scope="col">Created</th>
                    <th scope="col">Status</th>
                    <th scope="col">Edit</th>
                </tr>
                </thead>
                <tbody class="small">
                @foreach($reviews as $review)
                    <tr>
                        <th scope="row" align="center">{{ $loop->iteration }}</th>
                        <td width="120">{{ link_to_route('site.products.show', $review->product->title, ['slug' => $review->product->slug]) }}</td>
                        <td align="center">{{ $review->user->id }}</td>
                        <td>{{ $review->name }}</td>
                        <td align="center">{{ $review->rating }}</td>
                        <td>{{ $review->content }}</td>
                        <td>{{ $review->created_at }}</td>
                        <td align="center">
                            <a href="{{ route('admin.reviews.change_status', ['review' => $review]) }}">
                                <i class="fas {{ $review->active ? 'fa-check-square' : 'fa-square' }}"></i>
                            </a>
                        </td>
                        <td align="center">
                            <div class="d-flex">
                                <a href="{{ route('admin.reviews.edit', ['review' => $review]) }}"
                                   class="mx-1">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>

                                <a href="#" class="delete-link mx-1">
                                    <i class="far fa-trash-alt"></i>
                                </a>

                                {!! Form::open()->route('admin.reviews.destroy', ['review' => $review])
                                ->method('delete')
                                ->attrs(['style' => 'display: none;', 'class' => 'delete-form'])
                                 !!}
                                {!! Form::close() !!}
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{ $reviews->links() }}
        </div>
    </div>
@endsection