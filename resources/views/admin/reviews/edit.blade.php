@extends('layouts.admin')

@section('title','Edit review')

@section('content')
    <div class="card">
        <div class="card-header">
            {{ link_to_route('admin.reviews.index', '<', null, ['class' => 'btn btn-secondary btn-sm mr-2']) }}
            Edit review
        </div>

        <div class="card-body">
            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('success') }}

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            {!! Form::open()->method('patch')->route('admin.reviews.update', ['review' => $review])->fill($review) !!}
            {!! Form::text('name', 'Name')->required() !!}

            <rating value="{{ $review->rating }}"></rating>

            {!! Form::textarea('content', 'Text')->required() !!}

            @if($review->active === 1)
                {!! Form::radio('active', 'Active', 1)->attrs(['checked' => true]) !!}
                {!! Form::radio('active', 'Inactive', 0) !!}
            @elseif($review->active === 0)
                {!! Form::radio('active', 'Active', 1) !!}
                {!! Form::radio('active', 'Inactive', 0)->attrs(['checked' => true]) !!}
            @endif

            {!! Form::submit('Update review') !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection