@extends('layouts.admin')

@section('title', 'Start page')

@section('content')
    <div class="card">
        <div class="card-header">Info</div>

        <div class="card-body">
            <ul class="list-group">
                <li class="list-group-item">
                    {{  link_to_route('admin.products.index', 'Products') }}
                    <span class="badge badge-pill badge-secondary">{{ $products }}</span>
                </li>
                <li class="list-group-item">
                    {{  link_to_route('admin.categories.index', 'Categories') }}
                    <span class="badge badge-pill badge-secondary">{{ $categories }}</span>
                </li>
                <li class="list-group-item">
                    {{  link_to_route('admin.reviews.index', 'Reviews') }}
                    <span class="badge badge-pill badge-secondary">{{ $reviews }}</span>
                </li>
                <li class="list-group-item">
                    {{  link_to_route('admin.parse_items.index', 'Parse resources') }}
                    <span class="badge badge-pill badge-secondary">{{ $parse_items }}</span>
                </li>
            </ul>

            <check-price></check-price>
        </div>
    </div>
@endsection