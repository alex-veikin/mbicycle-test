@extends('layouts.site')

@section('title', 'Start page')

@section('content')
    <div class="card">
        <div class="card-header">Last {{ count($products) }} products</div>

        <div class="card-body">
            <div class="container">
                <div class="row">
                    @forelse($products as $product)
                        <div class="col-12 col-md-6 col-lg-4 mb-3">
                            @include('components.product-card', ['product' => $product])
                        </div>
                    @empty
                        <p class="list-group-item">No products</p>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
@endsection