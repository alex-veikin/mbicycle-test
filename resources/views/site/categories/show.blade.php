@extends('layouts.site')

@section('title', "Category - {$category->title}")

@section('content')
    <div class="card">
        <div class="card-header">Category: {{ $category->title }}</div>

        <div class="card-body">
            <div class="categories border-bottom">
                <h4>Subcategories:</h4>

                @if($category->children->count())
                    <div class="d-flex justify-content-center flex-wrap">
                        @forelse($category->children as $subcategory)
                            <div class="d-flex align-items-center m-1 p-1 border rounded">
                                <img class="" src="{{ $category->image_src }}"
                                     alt="{{ $category->title }}" style="display: block;width: 40px;height: auto;">

                                {{ link_to_route('site.categories.show', $subcategory->title,
                                ['segments' => $subcategory->urlSegments()]) }}

                            </div>
                        @empty
                            <p class="list-group-item">No subcategories</p>
                        @endforelse
                    </div>
                @else
                    <span>no subcategories</span>
                @endif
            </div>

            <div class="products mt-4">
                <h4>Products:</h4>

                @if($category->products->count())
                    <products url="{{ url()->current() }}"
                              path="{{ request()->path() }}"
                              category="{{ $category->slug }}"
                    />
                @else
                    <p>No products</p>
                @endif
            </div>
        </div>
    </div>
@endsection