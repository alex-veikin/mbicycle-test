@extends('layouts.site')

@section('title', "Categories")

@section('content')
    <div class="card">
        <div class="card-header">Categories</div>

        <div class="card-body">
            <div class="container">
                <div class="row">
                    @forelse($categories as $category)
                        <div class="col-12 col-md-6 col-lg-4 mb-3">
                            <div class="card text-center">
                                <img class="card-img-top img-thumbnail" src="{{ $category->image_src }}"
                                     alt="{{ $category->title }}" style="display: block;width: 100%;height: auto;">

                                <div class="card-body">
                                    {{
                                        link_to_route('site.categories.show_parent',
                                        "{$category->title} ({$category->children_count} subcategories)",
                                        ['parent' => $category->slug])
                                    }}
                                </div>
                            </div>

                        </div>
                    @empty
                        <p class="list-group-item">No categories</p>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
@endsection