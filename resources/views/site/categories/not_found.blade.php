@extends('layouts.site')

@section('title', 'Category not found')

@section('content')
    <div class="card">
        <div class="card-header">
            Category not found
        </div>

        <div class="card-body">
            <div class="container">
                Category "{{ $category_name }}" not found...
            </div>
        </div>
    </div>
@endsection