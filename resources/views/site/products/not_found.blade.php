@extends('layouts.site')

@section('title', 'Product not found')

@section('content')
    <div class="card">
        <div class="card-header">
            Product not found
        </div>

        <div class="card-body">
            <div class="container">
                Product "{{ $product_name }}" not found...
            </div>
        </div>
    </div>
@endsection