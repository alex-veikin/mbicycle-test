@extends('layouts.site')

@section('title', ('Product - ' . $product->title))
@section('description', $product->content)
@section('keywords', $product->metaKeywords())

@section('content')
    <div class="card">
        <div class="card-header">
            {{ link_to(url()->previous(), '<', ['class' => 'btn btn-secondary btn-sm mr-2']) }}
            Product: {{ $product->title }}
        </div>

        <div class="card-body">
            <div>
                <div class="d-flex justify-content-center">
                    <img class="card-img-top" src="{{ $product->image_src }}" alt="{{ $product->title }}"
                         style="display: block;width: 100%;height: auto;max-width: 300px;">
                </div>

                <div>
                    @forelse($product->categories as $category)
                        <p>
                            {{ link_to_route('site.categories.show',
                            $category->title,
                            ['segments' => $category->urlSegments()]) }}
                        </p>
                    @empty
                        No categories
                    @endforelse
                </div>

                <div>
                    Attributes:

                    <ul>
                        @forelse($product->attributes as $attribute)
                            <li>
                                {{ $attribute->title . ': ' . $attribute->description }}
                            </li>
                        @empty
                            No attributes
                        @endforelse
                    </ul>
                </div>

                <div class="mt-4">
                    Content:

                    <p>{{ $product->content }}</p>

                    @if($product->price)
                        <p>Price: ${{ $product->price }}</p>
                    @endif
                </div>
            </div>

            <div>
                @auth
                    @if(!$product->userHasReview())
                        <div class="mb-3">
                            {!! Form::open()->method('post')->route('site.products.store_reviews', ['product' => $product->id]) !!}
                            {!! Form::text('name', 'Name')->value(Auth::user()->name ?? '')->required() !!}

                            <rating></rating>

                            {!! Form::textarea('content', 'Text')->required() !!}
                            {!! Form::submit('Add review') !!}
                            {!! Form::close() !!}
                        </div>
                    @endif
                @endauth

                @if(session()->has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('success') }}

                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @elseif(session()->has('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ session('error') }}

                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                <div>
                    <div>Reviews:</div>

                    <div>
                        @if($product->activeReviews->count())
                            @foreach($product->activeReviews as $review)
                                <div class="border p-2 mt-2">
                                    <div>{{ $review->name }}</div>
                                    <rating value="{{ $review->rating }}" disabled="{{ true }}"></rating>
                                    <div>{{ $review->content }}</div>
                                </div>
                            @endforeach
                        @else
                            no reviews...
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection