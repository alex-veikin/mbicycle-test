@extends('layouts.site')

@section('title', 'Products')

@section('content')
    <div class="card">
        <div class="card-header">Products</div>

        <div class="card-body">
            <products url="{{ url()->current() }}" path="{{ request()->path() }}"/>
        </div>
    </div>
@endsection