<?php

namespace App\Services;
use Image;


class ImageService
{

    /**
     * Create an unique image name with a specific prefix
     *
     * @param      $image
     * @param null $prefix
     *
     * @return string
     */
    public static function createImageName( $image, $prefix = null ) {
        return ( $prefix ? $prefix . '_' : '' ) // prefix
               . round( microtime( true ) * 1000 ) // filename
               . '.'
               . $image->getClientOriginalExtension(); // extension
    }

    /**
     * Save the image to the specified path
     *
     * @param             $image
     * @param string      $image_name
     * @param int         $width
     * @param int         $height
     * @param string|null $path_name
     */
    public static function storeImage(
        $image,
        string $image_name,
        int $width,
        int $height,
        string $path_name = null
    ) {
        Image::make( $image->getRealPath() )
             ->fit( $width, $height, function ( $constraint ) {
                 $constraint->upsize();
             } )
             ->save( public_path('storage/img/' .
                           ($path_name ? $path_name : '')) . '/' . $image_name );
    }


    /**
     * @param null|string $image_name
     * @param string      $path
     *
     * @return string
     */
    public static function getImageSrc(?string $image_name, string $path) : string
    {
        // If has an image name
        if ($image_name) {
            $full_path = "img/$path/$image_name";

            // If exists a file in the storage return the full path to the file
            if (\Storage::disk('public')->exists($full_path)) {
                return asset("storage/$full_path");
            }
        }

        // Else return the path to the default file
        return asset('storage/img/no_image.png');
    }
}