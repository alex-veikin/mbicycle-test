<?php

namespace App\Services;

use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class ParserService
{

    protected $link;
    protected $useragent = 'Mozilla/5.0 (Windows NT 6.3; W…) Gecko/20100101 Firefox/57.0';
    protected $timeout = 100;
    protected $connect_timeout = 100;

    private $page = null;
    private $items = null;

    public function __construct(string $link)
    {
        $this->link = $link;
    }

    public function getPage()
    {
        $ch = curl_init($this->link);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Return data to value
        curl_setopt($ch, CURLOPT_HEADER, 1); // Return headers
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // Redirect
        curl_setopt($ch, CURLOPT_USERAGENT, $this->useragent); // Set UserAgent
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->connect_timeout);

        if (strpos($this->link, "https") !== false) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // HTTPS
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); // HTTPS
        }

        $page = curl_exec($ch);
        curl_close($ch);

        $this->page = $page;

        return $this;
    }


    public function getItems(string $selector)
    {
        $crawler = new Crawler(null, $this->link);
        $crawler->addHtmlContent($this->page, 'UTF-8');

        $items = $crawler->filter($selector)->each(function (Crawler $item) {
            return $item;
        });

        $this->items = $items;

        return $this;
    }

    public function extractItems(array $field_selectors)
    {
        $result_items = [];

        if (count($this->items)) {
            foreach ($this->items as $item) {
                $result_item = [];

                foreach ($field_selectors as $key => $selector) {
                    if ($key === 'image') {
                        $value = $item->filter($selector)
                                              ->first()
                                              ->attr('src');
                    } else {
                        $value = $item->filter($selector)
                                                  ->first()
                                                  ->text();

                        if ($key === 'price') {
                            preg_match('/\d+(?:\.\d{1,2})?/', $value, $matches);

                            $value = $matches[0];
                        }
                    }

                    $result_item[$key] = trim($value);

                }

                $result_items[] = $result_item;
            };
        }

        return $result_items;
    }

}