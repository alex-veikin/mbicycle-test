<?php

namespace App\Services;


use App\Mail\ExportExcelMail;
use App\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;
use Rap2hpoutre\FastExcel\FastExcel;

class ExportService
{

    protected $excel;
    protected $structure;
    protected $filename = 'export.xlsx';

    public function __construct(Collection $collection)
    {
        $this->excel = new FastExcel($collection);
    }

    /**
     * @param callable $callback
     *
     * @return $this
     */
    public function structure(callable $callback)
    {
        $this->structure = $callback;

        return $this;
    }

    /**
     * @param bool   $bold
     * @param int    $font_size
     * @param string $font_color
     * @param bool   $text_wrap
     * @param string $background_color
     *
     * @return \Rap2hpoutre\FastExcel\Exportable
     */
    public function setHeaderStyle(
        bool $bold = true,
        int $font_size = 15,
        string $font_color = 'Color:BLACK',
        bool $text_wrap = true,
        string $background_color = 'Color:WHITE'
    ) {
        return $this->excel->headerStyle(
            $bold,
            $font_size,
            $font_color,
            $text_wrap,
            $background_color
        );
    }

    /**
     * @param string $filename
     *
     * @return string|\Symfony\Component\HttpFoundation\StreamedResponse
     * @throws \Box\Spout\Common\Exception\IOException
     * @throws \Box\Spout\Common\Exception\InvalidArgumentException
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     * @throws \Box\Spout\Writer\Exception\WriterNotOpenedException
     */
    public function download(string $filename = null)
    {
        return $this->excel->download($filename ?? $this->filename,
            $this->structure);
    }

    /**
     * @param string|null $filename
     *
     * @return string
     * @throws \Box\Spout\Common\Exception\IOException
     * @throws \Box\Spout\Common\Exception\InvalidArgumentException
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     * @throws \Box\Spout\Writer\Exception\WriterNotOpenedException
     */
    public function export(string $filename = null)
    {
        return $this->excel->export($filename ?? $this->filename,
            $this->structure);
    }

    /**
     * @param \App\User   $user
     * @param string|null $filename
     *
     * @throws \Box\Spout\Common\Exception\IOException
     * @throws \Box\Spout\Common\Exception\InvalidArgumentException
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     * @throws \Box\Spout\Writer\Exception\WriterNotOpenedException
     */
    public function toEmail(User $user, string $filename = null)
    {
        $file = $this->store('export' . time() . '.xlsx');

        Mail::to($user)->send(new ExportExcelMail($file,
            $filename ?? 'export.xlsx'));
    }

    /**
     * @param string|null $filename
     *
     * @return string
     * @throws \Box\Spout\Common\Exception\IOException
     * @throws \Box\Spout\Common\Exception\InvalidArgumentException
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     * @throws \Box\Spout\Writer\Exception\WriterNotOpenedException
     */
    public function store(string $filename = null)
    {
        $path = 'export/' . ($filename ?? $this->filename);

        $this->export(public_path('storage/' . $path));
        return $path;
    }

}