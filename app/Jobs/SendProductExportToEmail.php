<?php

namespace App\Jobs;

use App\Services\ExportService;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Collection;

class SendProductExportToEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;

    protected $user;
    protected $products;

    /**
     * SendProductExportToEmail constructor.
     *
     * @param \App\User                      $user
     * @param \Illuminate\Support\Collection $products
     */
    public function __construct(User $user, Collection $products)
    {
        $this->user     = $user;
        $this->products = $products;
    }

    /**
     * @throws \Box\Spout\Common\Exception\IOException
     * @throws \Box\Spout\Common\Exception\InvalidArgumentException
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     * @throws \Box\Spout\Writer\Exception\WriterNotOpenedException
     */
    public function handle()
    {
        $export = new ExportService($this->products);

        $export->structure(function ($product) {
            return [
                'Title' => $product->title,
                'Description' => $product->content,
                'Price' => $product->price,
                'Created' => $product->created_at,
            ];
        });

        $export->toEmail($this->user, 'products.xlsx');
    }
}
