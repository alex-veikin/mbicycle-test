<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Storage;

class ExportExcelMail extends Mailable
{

    use Queueable, SerializesModels;

    public $file;
    public $file_name;

    /**
     * ExportExcelMail constructor.
     *
     * @param        $file
     * @param string $file_name
     */
    public function __construct($file, string $file_name)
    {
        $this->file      = $file;
        $this->file_name = $file_name;
    }

    /**
     * @return \App\Mail\ExportExcelMail
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function build()
    {
        return $this->view('emails.export-excel')
                    ->attachData(Storage::get('public/' . $this->file),
                        $this->file_name, [
                            'mime' => 'application/vnd.ms-excel',
                        ]);
    }
}
