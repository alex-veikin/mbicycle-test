<?php

namespace App;

use App\Filters\FilterScope;
use App\Services\ImageService;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Product extends Model
{

    use FilterScope;

    protected $fillable = ['slug', 'title', 'content', 'image', 'price'];
    protected $appends = ['image_src'];

    // Product categories (many-to-many)
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    // Product attributes (one-to-many)
    public function attributes()
    {
        return $this->hasMany(ProductAttribute::class);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function activeReviews()
    {
        return $this->reviews()->where('active', '=', 1);
    }

    public function userHasReview()
    {
        return $this->reviews()
                    ->where('user_id', '=', Auth::id())
                    ->count();
    }

    public function getImageSrcAttribute()
    {
        return ImageService::getImageSrc($this->image, 'products');
    }

    public function metaKeywords()
    {
        $keywords = preg_split("/[\s,.;:'\"()+]+/", $this->content);

        return implode(',', array_slice($keywords, 0, 10));
    }
}
