<?php

namespace App;

use App\Services\ImageService;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['parent_id', 'slug', 'title', 'image'];
    protected $appends = ['image_src'];
    protected $with = ['parent'];

    // Products in current category
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }


    // Parent category of current
    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }


    // Children categories of current
    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    // Segments string of slugs ('slug-parent/slug-child/slug-child2/...')
    public function urlSegments()
    {
        $segments = '';

        if($this->parent) {
            $segments .= $this->parent->urlSegments() . '/' ;
        }

        return $segments . $this->slug;
    }

    // Scope isParent
    public function scopeIsParent($query)
    {
        return $query->whereNull('parent_id');
    }

    public function getImageSrcAttribute()
    {
        return ImageService::getImageSrc($this->image, 'products');
    }
}
