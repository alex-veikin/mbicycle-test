<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PATCH') {
            return [
                'title'      => 'required|string|max:255|unique:products,title,'
                                . $this->product->id
                                . '|unique:categories,title',
                'slug'       => 'required|string|max:255|'
                                . 'regex:/^[a-z0-9]+(?:-[a-z0-9]+)*$/|'
                                . 'unique:products,slug,' . $this->product->id
                                . '|unique:categories,slug',
                'content'    => 'required',
                'image'      => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'fields' => 'array',
                'product_categories' => 'array',
            ];
        } else {
            return [
                'title'      => 'required|string|max:255|unique:products,title|'
                                . 'unique:categories,title',
                'slug'       => 'required|string|max:255|'
                                . 'regex:/^[a-z0-9]+(?:-[a-z0-9]+)*$/|'
                                . 'unique:products,slug|unique:categories,slug',
                'content'    => 'required',
                'image'      => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'fields' => 'array',
                'product_categories' => 'array',
            ];

        }
    }
}
