<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ParseItemRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PATCH') {
            return [
                'title'    => 'required|string|max:255|unique:parse_items,title,'
                              . $this->parse_item->id,
                'type'     => 'required|string|max:255',
                'url'      => 'required|url|max:255',
                'selector' => 'required|string|max:255',
                'fields'   => 'array',
            ];
        } else {
            return [
                'title'   => 'required|string|max:255|unique:parse_items,title',
                'type'     => 'required|string|max:255',
                'url'      => 'required|url|max:255',
                'selector' => 'required|string|max:255',
                'fields'  => 'array',
            ];

        }
    }
}
