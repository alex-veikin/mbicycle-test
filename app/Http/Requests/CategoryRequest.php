<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PATCH') {
            return [
                'parent_id' => 'integer|exists:categories,id',
                'title'     => 'required|string|max:255|unique:categories,title,'
                               . $this->category->id . '|unique:products,title',
                'slug'      => 'required|string|max:255|'
                               . 'regex:/^[a-z0-9]+(?:-[a-z0-9]+)*$/|'
                               . 'unique:categories,slug,' . $this->category->id
                               . '|unique:products,slug',
                'image'     => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ];
        } else {
            return [
                'parent_id' => 'integer|exists:categories,id',
                'title'     => 'required|string|max:255|unique:categories,title'
                               . '|unique:products,title',
                'slug'      => 'required|string|max:255|'
                               . 'regex:/^[a-z0-9]+(?:-[a-z0-9]+)*$/|'
                               . 'unique:categories,slug|unique:products,slug',
                'image'     => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ];

        }
    }
}
