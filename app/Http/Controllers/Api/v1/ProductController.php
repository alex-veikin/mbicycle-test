<?php

namespace App\Http\Controllers\Api\v1;

use App\Product;
use App\Services\ExportService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{

    public function index(Request $request)
    {
        $products = Product::filter()
                           ->latest()
                           ->paginate(6)
                           ->appends($request->all());

        return response()->json($products);
    }

    public function admin(Request $request)
    {
        $products = Product::with(['categories.parent', 'attributes'])
                           ->filter()
                           ->latest()
                           ->paginate(10)
                           ->appends($request->all());

        return response()->json($products);
    }

    public function delete($id)
    {
        Product::find($id)->delete();
    }

    public function checkPrice(Request $request)
    {
        $more_query = Product::where('price', '>', $request->price);
        $more_sum = $more_query->sum('price');
        $more = [
            'count' => $more_query->count(),
            'sum'   => $more_sum,
            'sum_text' => digit_text($more_sum, 'ru', true),
        ];

        $less_query = Product::where('price', '<', $request->price);
        $less_sum = $less_query->sum('price');
        $less = [
            'count' => $less_query->count(),
            'sum'   => $less_sum,
            'sum_text' => digit_text($less_sum, 'ru', true),
        ];

        return response()->json([
            'more' => $more,
            'less' => $less,
            'price_text' => digit_text($request->price, 'ru', true)
        ]);
    }
}
