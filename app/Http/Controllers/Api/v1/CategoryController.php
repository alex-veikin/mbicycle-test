<?php

namespace App\Http\Controllers\Api\v1;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{


    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request)
    {
        $category = null;

        // Get uri segments after
        $uri_segments = explode('/', $request->segments);

        foreach ($uri_segments as $key => $value) {
            if ($key === 0) {
                // Get parent category
                $category = Category::isParent()->whereSlug($value)->with([
                    'children' => function ($query) {
                        $query->withCount('products');
                        $query->withCount('children');
                    },
                ])->first();
            } else {
                // Get subcategory
                $subcategory = $category->children()->whereSlug($value)->with([
                    'children' => function ($query) {
                        $query->withCount('products');
                        $query->withCount('children');
                    },
                ])->first();

                $category = $subcategory;
            }
        }

        $products = $category->products()
                             ->filter()
                             ->latest()
                             ->paginate(6)
                             ->appends($request->all());

        return response()->json($products);
    }
}
