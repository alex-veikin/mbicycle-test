<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ParseItemRequest;
use App\ParseItem;
use App\Services\ParserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ParserController extends Controller
{

    public function index()
    {
        $parse_items = ParseItem::all();

        return view('admin.parser.index', compact('parse_items'));
    }

    public function show(ParseItem $parse_item)
    {
        $parser = new ParserService($parse_item->url);
        $items  = $parser->getPage()
                         ->getItems($parse_item->selector)
                         ->extractItems($parse_item->fields_array);

        return view('admin.parser.show', compact('parse_item', 'items'));
    }

    public function create()
    {
        return view('admin.parser.create');
    }

    public function store(ParseItemRequest $request)
    {
        $request->merge(['fields' => json_encode($request->fields)]);

        ParseItem::create($request->all());

        return view('admin.parser.create');
    }

    public function edit(ParseItem $parse_item)
    {
        return view('admin.parser.edit', compact('parse_item'));
    }

    public function update(ParseItemRequest $request, ParseItem $parse_item)
    {
        $request->merge(['fields' => json_encode($request->fields)]);

        $parse_item->update($request->all());

        return redirect()->back()->with('success', 'Resource updated successfully!');
    }

    public function destroy(ParseItem $parse_item)
    {
        $parse_item->delete();

        return redirect()->back()->with('success', 'Resource deleted successfully!');
    }
}
