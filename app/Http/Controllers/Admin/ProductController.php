<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Requests\ProductRequest;
use App\Product;
use App\Services\ImageService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{

    /**
     * Show all products
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $products = Product::with(['categories.parent', 'attributes'])
                           ->filter()
                           ->latest()
                           ->paginate(10)
                           ->appends($request->all());

        return view('admin.products.index', compact('products'));
    }

    /**
     * Create page
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::whereNull('parent_id')
                              ->with('children')
                              ->get();

        return view('admin.products.create', compact('categories'));
    }

    /**
     * Save product
     *
     * @param \App\Http\Requests\ProductRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ProductRequest $request)
    {
        // If request has a file
        if ($request->hasFile('file')) {
            $image = $request->file('file');

            // Create an image name
            $image_name = ImageService::createImageName($image, 'product');

            // Store the file
            ImageService::storeImage(
                $image,
                $image_name,
                400,
                300,
                'products'
            );
        }

        // Add a field to the request array
        $request->request->add(['image' => $image_name ?? null]);

        // Save the product
        /** @var Product $product */
        $product = Product::create($request->all());

        // If request has product attributes
        if ($request->fields) {
            // Create array
            $attributes = [];

            // Adding attributes to array
            foreach ($request->fields as $key => $value) {
                array_push($attributes, [
                    'title'       => $key,
                    'description' => $value,
                ]);
            }

            // Create attributes in database
            $product->attributes()->createMany($attributes);
        }

        // If request has categories
        if ($request->product_categories) {
            // Attach categories to product
            $product->categories()->sync($request->product_categories);
        }

        // Redirect to products page
        return redirect()->route('admin.products.index')
                         ->with("success", "Product created successfully!");
    }

    /**
     * Edit page
     *
     * @param \App\Product $product
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Product $product)
    {
        $categories = Category::whereNull('parent_id')
                              ->with('children')
                              ->get();

        return view('admin.products.edit',
            compact('categories', 'product'));
    }

    /**
     * Update product
     *
     * @param \App\Http\Requests\ProductRequest $request
     * @param \App\Product                      $product
     *
     * @return mixed
     */
    public function update(ProductRequest $request, Product $product)
    {
        // If request has a file
        if ($request->hasFile('file')) {
            $image = $request->file('file');

            // Create an image name
            $image_name = ImageService::createImageName($image, 'product');

            // Store the file
            ImageService::storeImage(
                $image,
                $image_name,
                400,
                300,
                'products'
            );
        } elseif ($request->query('image')) {

            $image = $request->query('image');

            // Create an image name
            $image_name = ImageService::createImageName($image, 'product');

            // Store the file
            ImageService::storeImage(
                $image,
                $image_name,
                400,
                300,
                'products'
            );
        }

        // Add a field to the request array
        $request->request->add([
            'image' => $image_name ?? $product->image ?? null,
        ]);

        // Update the product
        /** @var Product $product */
        $product->update($request->all());

        // If request has product attributes
        if ($request->fields) {
            // Delete attributes
            $product->attributes()
                    ->whereNotIn('title',
                        array_keys($request->fields))
                    ->delete();

            // Adding attributes to array
            foreach ($request->fields as $key => $value) {
                // Update or update attribute in database
                $product->attributes()->updateOrCreate(
                    ['title' => $key],
                    ['description' => $value]
                );
            }
        }

        // If request has categories
        if ($request->product_categories) {
            // Attach categories to product
            $product->categories()->sync($request->product_categories);
        }

        // Redirect to products page
        return redirect()->back()
                         ->with("success", "Product updated successfully!");
    }

    /**
     * Delete product
     *
     * @param \App\Product $product
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('admin.products.index')
                         ->with('success', 'Product deleted successfully!');
    }
}
