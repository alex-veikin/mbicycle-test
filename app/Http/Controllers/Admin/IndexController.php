<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\ParseItem;
use App\Product;
use App\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{

    public function index()
    {
        $products    = Product::count();
        $categories  = Category::count();
        $reviews     = Review::count();
        $parse_items = ParseItem::count();

        return view('admin.index', compact([
            'products',
            'categories',
            'reviews',
            'parse_items'
        ]));
    }
}
