<?php

namespace App\Http\Controllers\Admin;

use App\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reviews = Review::with(['user', 'product'])
                         ->latest()
                         ->paginate(20);

        return view('admin.reviews.index', compact('reviews'));
    }

    /**
     * @param \App\Review $review
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Review $review)
    {
        return view('admin.reviews.edit', compact('review'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Review              $review
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Review $review)
    {
        $review->update($request->all());

        return redirect()->back()
                         ->with("success", "Review updated successfully!");
    }

    /**
     * @param \App\Review              $review
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeStatus(Review $review)
    {
        $review->update(['active' => $review->active === 1 ? 0 : 1]);

        return redirect()->back()
                         ->with("success", "Status changed!");
    }

    /**
     * @param \App\Review $review
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Review $review)
    {
        $review->delete();

        return redirect()->route('admin.reviews.index')
                         ->with('success', 'Review deleted successfully!');
    }
}
