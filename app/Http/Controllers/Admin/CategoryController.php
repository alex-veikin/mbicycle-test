<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Requests\CategoryRequest;
use App\Services\ImageService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{

    /**
     * Show all categories
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $categories = Category::with('children')->whereNull('parent_id')->get();

        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show category with subcategories
     *
     * @param \App\Category $category
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show(Category $category)
    {
        if ($category->parent_id) {
            return redirect()->back();
        }

        return view('admin.categories.show',
            ['category' => $category->load('children')]);
    }

    /**
     * Create page
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(Request $request)
    {
        if ($request->parent) {
            $category = Category::find($request->parent);

            if (!$category || $category->parent) {
                return redirect()->route('admin.categories.index');
            }

            return view('admin.categories.create', compact('category'));
        }

        return view('admin.categories.create');
    }

    /**
     * Save category
     *
     * @param \App\Http\Requests\CategoryRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CategoryRequest $request)
    {
        // If request has a file
        if ($request->hasFile('file')) {
            $image = $request->file('file');

            // Create an image name
            $image_name = ImageService::createImageName($image, 'category');

            // Store the file
            ImageService::storeImage(
                $image,
                $image_name,
                400,
                300,
                'categories'
            );
        }

        // Add a field to the request array
        $request->request->add(['image' => $image_name ?? null]);

        // Store the category
        Category::create($request->all());

        if ($request->has('parent_id')) {
            // Redirect to category page
            return redirect()->route('admin.categories.show',
                ['category' => $request->parent_id])
                             ->with("success", "Subcategory created successfully!");
        } else {
            // Redirect to categories page
            return redirect()->route('admin.categories.index')
                             ->with("success", "Category created successfully!");
        }
    }

    /**
     * Edit page
     *
     * @param \App\Category $category
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Category $category)
    {
        return view('admin.categories.edit', compact('category'));
    }

    /**
     * Update category
     *
     * @param \App\Http\Requests\CategoryRequest $request
     * @param \App\Category                      $category
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CategoryRequest $request, Category $category)
    {
        // If request has a file
        if ($request->hasFile('file')) {
            $image = $request->file('file');

            // Create an image name
            $image_name = ImageService::createImageName($image, 'category');

            // Store the file
            ImageService::storeImage(
                $image,
                $image_name,
                400,
                300,
                'categories'
            );
        }

        // Add a field to the request array
        $request->request->add(['image' => $image_name ?? $category->image ?? null]);

        // Store the category
        $category->update($request->all());

        // Redirect to products page
        return redirect()->back()
                         ->with("success", "Category updated successfully!");

    }

    /**
     * Delete category
     *
     * @param \App\Category $category
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Category $category)
    {
        $parent_id = $category->parent_id;

        $category->delete();

        if ($parent_id) {
            return redirect()->route('admin.categories.show',
                ['category' => $parent_id])
                             ->with('success', 'Subcategory deleted successfully!');
        } else {
            return redirect()->route('admin.categories.index')
                             ->with('success', 'Category deleted successfully!');

        }
    }

}
