<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $categories = Category::whereNull('parent_id')
                              ->withCount('children')
                              ->get();

        return view('site.categories.index', compact('categories'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request)
    {
        $category = null;

        // Get uri segments after
        $uri_segments = explode('/', $request->segments);

        foreach ($uri_segments as $key => $value) {
            if ($key === 0) {
                // Get parent category
                $category = Category::isParent()->whereSlug($value)->with([
                    'children' => function ($query) {
                        $query->withCount('products');
                        $query->withCount('children');
                    },
                ])->first();
            } else {
                // Get subcategory
                $subcategory = $category->children()->whereSlug($value)->with([
                    'children' => function ($query) {
                        $query->withCount('products');
                        $query->withCount('children');
                    },
                ])->first();

                // If subcategory not found and segment is latest
                if ( ! $subcategory && (count($uri_segments) == $key + 1)) {
                    // Get product
                    $product = $category->products()->whereSlug($value)->first();

                    // Show product if found
                    if ($product) {
                        return view('site.products.show',
                            compact('product'));
                    }
                }

                $category = $subcategory;
            }

            // If category not found
            if ( ! $category) {
                return view('site.categories.not_found',
                    ['category_name' => $value]);
            }
        }

        return view('site.categories.show', compact('category'));
    }
}
