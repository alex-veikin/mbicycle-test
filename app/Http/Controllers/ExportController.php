<?php

namespace App\Http\Controllers;

use App\Category;
use App\Jobs\SendProductExportToEmail;
use App\Product;
use App\Services\ExportService;
use Illuminate\Http\Request;

class ExportController extends Controller
{


    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|string|\Symfony\Component\HttpFoundation\StreamedResponse
     * @throws \Box\Spout\Common\Exception\IOException
     * @throws \Box\Spout\Common\Exception\InvalidArgumentException
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     * @throws \Box\Spout\Writer\Exception\WriterNotOpenedException
     */
    public function exportProducts(Request $request)
    {
        $products = null;

        if ($request->query('category')) {
            $category = Category::whereSlug($request->query('category'))
                                ->first();

            if ($category) {
                $products = $category->products()->filter()->latest()->get();
            }
        } else {
            $products = Product::filter()->latest()->get();
        }

        if ($products) {
            if ($request->query('method') === 'email') {
                SendProductExportToEmail::dispatch($request->user(), $products);

                return back();
            } elseif ($request->query('method') === 'download') {
                $export = new ExportService($products);

                $export->structure(function ($product) {
                    return [
                        'Title' => $product->title,
                        'Description' => $product->content,
                        'Price' => $product->price,
                        'Created' => $product->created_at,
                    ];
                });

                return $export->download('products.xlsx');
            }
        }
    }
}
