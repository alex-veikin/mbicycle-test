<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('site.products.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request)
    {
        $product = Product::whereSlug($request->product)
                          ->with('categories.parent')
                          ->first();

        if ( ! $product ) {
            return view('site.products.not_found',
                ['product_name' => $request->product]);
        }

        return view('site.products.show', compact('product'));
    }
}
