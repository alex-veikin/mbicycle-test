<?php

namespace App\Http\Controllers;

use App\Product;
use App\Review;
use Illuminate\Http\Request;
use Auth;

class ReviewController extends Controller
{

    /**
     * @param \Illuminate\Http\Request $request
     * @param                          $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, $id)
    {
        $product = Product::find($id);

        if ($product) {
            $request->merge(['user_id' => Auth::id()]);

            $product->reviews()->create($request->all());

            return redirect()->back()->with('success', 'You review saved.');
        }

        return redirect()->back()->with('error', 'An error has occurred.');
    }
}
