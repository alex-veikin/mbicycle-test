<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;

class IndexController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products  = Product::orderBy('created_at', 'desk')
                            ->take(6)
                            ->get();

        return view('site.index', compact('products'));
    }
}
