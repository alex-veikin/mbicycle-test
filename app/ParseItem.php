<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParseItem extends Model
{
    protected $fillable = ['title', 'type', 'url', 'selector', 'fields'];
    protected $appends = ['fields_array'];

    public function getFieldsArrayAttribute()
    {
        return json_decode($this->fields, true);
    }
}
