<?php

namespace App\Filters;

class ProductFilter extends Filter
{

    public function image($value)
    {
        if ($value === 'true') {
            return $this->query->whereNotNull('image');
        }

        return $this->query;
    }

    public function orderBy($value)
    {
        if ($this->request->sort === 'desc') {
            return $this->query->orderByDesc($value);
        } else {
            return $this->query->orderBy($value);
        }
    }

    public function search($value)
    {
        if ($value) {
            $words = explode(' ', $value);

            return $this->query->where(function ($query) use ($words) {
                $query->orWhere(function ($query) use ($words) {
                    foreach ($words as $word) {
                        $query->where('title', 'like',
                            '%' . trim($word, ' ,.;\'"<>\n\r') . '%');
                    }
                })->orWhere(function ($query) use ($words) {
                    foreach ($words as $word) {
                        $query->where('content', 'like',
                            '%' . trim($word, ' ,.;\'"<>\n\r') . '%');
                    }
                });
            });
        }

        return $this->query;
    }

}