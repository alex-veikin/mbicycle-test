<?php

namespace App\Filters;

use Illuminate\Http\Request;

abstract class Filter
{

    protected $request;
    protected $query;

    public function __construct(Request $request, $query)
    {
        $this->request = $request;
        $this->query   = $query;
    }

    /**
     * Get all filters from the query
     *
     * @return array
     */
    public function getFilters()
    {
        return $this->request->query();
    }


    /**
     * Apply filters to the query
     *
     * @param $query
     *
     * @return mixed
     */
    public function apply()
    {
        foreach ($this->getFilters() as $filter_name => $value) {
            $method_name = camel_case($filter_name);

            // Call filter if exists by name
            if (method_exists($this, $method_name)) {
                $this->$method_name($value);
            }
        }

        return $this->query;
    }

}