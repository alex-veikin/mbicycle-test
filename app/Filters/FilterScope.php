<?php

namespace App\Filters;

trait FilterScope
{

    /**
     * Scope filter for model
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeFilter($query)
    {
        // Get the current model name with namespace
        $model = get_class($this);

        // Make a filter name with namespace
        $filter = 'App\\Filters\\' . class_basename($model) . 'Filter';

        // If filter class exists
        if (class_exists($filter)) {
            // Apply filters from the current query string
            return (new $filter(request(), $query))->apply();
        }

        return $query;
    }
}