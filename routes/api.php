<?php

Route::group(['prefix' => 'v1', 'namespace' => 'Api\v1'], function () {
    Route::get('products', 'ProductController@index');
    Route::post('products/check-price', 'ProductController@checkPrice');

    Route::get('admin/products', 'ProductController@admin');
    Route::delete('admin/products/{id}', 'ProductController@delete');

    Route::group(['prefix' => 'categories'], function () {
        Route::get('{segments}', 'CategoryController@show')
             ->where('segments', '.*');
    });
});
