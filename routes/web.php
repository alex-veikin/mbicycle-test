<?php

// Site routes
Route::group(['as' => 'site.'], function () {
    // Start page
    Route::get('/', 'IndexController@index')->name('index');

    // Products routes
    Route::group([
        'prefix' => 'products',
        'as'     => 'products.',
    ], function () {
        Route::get('/', 'ProductController@index')
             ->name('index');
        Route::get('{product}', 'ProductController@show')
             ->name('show');
        Route::post('{product}/reviews', 'ReviewController@store')
             ->name('store_reviews');
    });

    // Categories routes
    Route::group([
        'prefix' => 'categories',
        'as'     => 'categories.',
    ], function () {
        Route::get('/', 'CategoryController@index')
             ->name('index');
        Route::get('{segments}', 'CategoryController@show')
             ->where('segments', '.*')->name('show');
    });
});


// Admin panel routes
Route::group([
    'prefix'     => 'admin',
    'namespace'  => 'Admin',
    'middleware' => 'auth',
    'as'         => 'admin.',
], function () {
    // Start page
    Route::get('/', 'IndexController@index')->name('index');

    // Products routes
    Route::resource('products', 'ProductController')
         ->except(['show']);

    // Reviews routes
    Route::resource('reviews', 'ReviewController')
         ->except(['show', 'create', 'store']);
    Route::get('reviews/{review}/change_status', 'ReviewController@changeStatus')
         ->name('reviews.change_status');

    // Categories routes
    Route::resource('categories', 'CategoryController');

    // Parser
    Route::resource('parse_items', 'ParserController');
});


// Export to excel
Route::get('export/products', 'ExportController@exportProducts')
     ->name('export.products');


Auth::routes();

// Redirect if route is register
Route::match(['get', 'post'], '/register', function () {
    return redirect()->route('site.index');
});
