<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Product::class, 40)
            ->create()
            ->each(function (\App\Product $product) {
                // Get random parent category
                /** @var \App\Category $category */
                $category = \App\Category::whereNull('parent_id')
                                         ->get()
                                         ->random();

                // Get list of children categories from the parent category
                $categories = $category->children()
                                       ->pluck('id')
                                       ->toArray();

                // Attach several categories to each products
                $product->categories()
                        ->sync(array_random($categories, random_int(1, 3)));
            });
    }
}
