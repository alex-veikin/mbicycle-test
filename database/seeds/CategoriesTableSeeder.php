<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create five parent categories
        factory(\App\Category::class, 5)
            ->create()
            ->each(function (\App\Category $category) {
                // Create several children categories to each parent category
                $category
                    ->children()
                    ->saveMany(factory(\App\Category::class, random_int(3, 4))
                        ->make());
            });
    }
}
