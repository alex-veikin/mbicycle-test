<?php

use Faker\Generator as Faker;

$factory->define(\App\Product::class, function (Faker $faker) {
    $title = $faker->words(random_int(2, 3), true);

    return [
        'slug'    => str_replace(' ', '-', $title),
        'title'   => ucfirst($title),
        'content' => $faker->text(100),
        'price'   => $faker->randomFloat(2, 10, 100)
    ];
});
